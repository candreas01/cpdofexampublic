package com.agiletestingalliance;

public class MinMax {

    public int selectMax(int firstNo, int secondNo) {
        if (secondNo > firstNo) {
            return secondNo;
        }
        else {
            return firstNo;
        }
    }

}
