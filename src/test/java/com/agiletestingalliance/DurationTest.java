package com.agiletestingalliance;

import com.agiletestingalliance.Duration;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class DurationTest {

    @Test
    public void runDur(){
        Duration duration = new Duration();
        String str = "CP-DOF is designed specifically for corporates and working professionals alike. If you are a corporate and can't dedicate full day for training, then you can opt for either half days course or  full days programs which is followed by theory and practical exams.";
        assertEquals(str, duration.dur());
    }
}
