package com.agiletestingalliance;

import com.agiletestingalliance.MinMax;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class MinMaxTest {

    @Test
    public void runMinMaxIfMet(){
        MinMax minmax = new MinMax();

        int firstNo = 1;
        int secondNo = 2;

        assertEquals(secondNo, minmax.selectMax(firstNo, secondNo));
    }

    @Test
    public void runMinMaxElseMet(){
        MinMax minmax = new MinMax();

        int firstNo = 3;
        int secondNo = 2;

        assertEquals(firstNo, minmax.selectMax(firstNo, secondNo));
    }

}
